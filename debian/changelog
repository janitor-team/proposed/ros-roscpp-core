ros-roscpp-core (0.7.2-8) unstable; urgency=medium

  * Team upload.
  * Upload to unstable
  * Fix Multiarch hinter issues

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:44:26 +0200

ros-roscpp-core (0.7.2-7) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Thu, 02 Sep 2021 20:49:14 +0200

ros-roscpp-core (0.7.2-6) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 11:50:29 +0100

ros-roscpp-core (0.7.2-5) unstable; urgency=medium

  * Minimize Boost dependency (Closes: #977282)
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Dec 2020 21:50:43 +0100

ros-roscpp-core (0.7.2-4) unstable; urgency=medium

  * Temporary change librostime version
  * Upload to unstable.

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 29 Jun 2020 21:49:21 +0200

ros-roscpp-core (0.7.2-3) experimental; urgency=medium

  * Revert "Add patch to revert to old rostime ABI"
  * Bump Soname due to ABI change

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 11 Jun 2020 14:34:08 +0200

ros-roscpp-core (0.7.2-2) unstable; urgency=medium

  * Add patch to revert to old rostime ABI

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 11 Jun 2020 13:11:25 +0200

ros-roscpp-core (0.7.2-1) unstable; urgency=medium

  * simplify d/watch
  * New upstream version 0.7.2
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 11:14:13 +0200

ros-roscpp-core (0.6.13-1) unstable; urgency=medium

  * New upstream version 0.6.13
  * Bump debhelper version

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Oct 2019 19:40:25 +0200

ros-roscpp-core (0.6.12-1) unstable; urgency=medium

  * New upstream version 0.6.12
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * cleanup rules
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 31 Jul 2019 13:17:07 +0200

ros-roscpp-core (0.6.11-2) unstable; urgency=medium

  * Drop python build dependency (not used)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 21 Jun 2018 21:16:33 +0200

ros-roscpp-core (0.6.11-1) unstable; urgency=medium

  * New upstream version 0.6.11

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 16 Jun 2018 16:28:36 +0200

ros-roscpp-core (0.6.10-1) unstable; urgency=medium

  * New upstream version 0.6.10
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 27 May 2018 12:25:26 +0200

ros-roscpp-core (0.6.9-1) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * Add R³
  * http -> https
  * New upstream version 0.6.9
  * Update policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 31 Mar 2018 22:41:05 +0200

ros-roscpp-core (0.6.7-1) unstable; urgency=medium

  * New upstream version 0.6.7

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Nov 2017 13:03:37 +0100

ros-roscpp-core (0.6.6-1) unstable; urgency=medium

  * New upstream version 0.6.6
  * Remove patch, merged upstream
  * Update standards version

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 28 Oct 2017 10:16:54 +0200

ros-roscpp-core (0.6.5-2) unstable; urgency=medium

  * Add patch for Hurd
  * new standards version (no changes needed)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 26 Sep 2017 10:46:35 +0200

ros-roscpp-core (0.6.5-1) unstable; urgency=medium

  * New upstream version 0.6.5

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 12 Aug 2017 23:32:06 +0200

ros-roscpp-core (0.6.4-1) unstable; urgency=medium

  * add Multi-Arch according to hinter
  * New upstream version 0.6.4
  * Update policy and debhelper versions
  * Update watch file
  * Remove parallel (Default for debhelper 10)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 08 Jul 2017 07:36:36 +0200

ros-roscpp-core (0.6.1-2) unstable; urgency=medium

  * Set priority to optional to make deb check happy

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 26 Dec 2016 18:19:57 +0100

ros-roscpp-core (0.6.1-1) unstable; urgency=medium

  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update URLs
  * Update my email address
  * New upstream version 0.6.1

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 18 Sep 2016 09:26:36 +0200

ros-roscpp-core (0.6.0-1) unstable; urgency=medium

  * Imported Upstream version 0.6.0
  * Refresh patches

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 18 Jun 2016 11:22:44 +0200

ros-roscpp-core (0.5.6-2) unstable; urgency=medium

  * Convert to new catkin with multiarch

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 24 Nov 2015 12:47:27 +0100

ros-roscpp-core (0.5.6-1) unstable; urgency=medium

  * Initial release. (Closes: #804030)

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Mon, 16 Nov 2015 03:53:46 +0000
