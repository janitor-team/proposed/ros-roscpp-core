Source: ros-roscpp-core
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13), catkin (>= 0.8.10-1~), libboost-dev, libconsole-bridge-dev, libgtest-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/ros-roscpp-core
Vcs-Git: https://salsa.debian.org/science-team/ros-roscpp-core.git
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/cpp_common

Package: libroscpp-core-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcpp-common0d (= ${binary:Version}), libroscpp-serialization0d (= ${binary:Version}), librostime1d (= ${binary:Version}), libboost-dev, libconsole-bridge-dev, ${misc:Depends}
Description: Development files for Robot OS roscpp-core
 This package is part of Robot OS (ROS). It contains the development
 files for roscpp_core which is an underlying library that supports
 roscpp message data types. It is a lightweight/minimal library that
 can easily be used in non-ROS-based projects.

Package: libcpp-common0d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Robot OS utility library
 This package is part of Robot OS (ROS). It contains the C++ library
 libcpp_common which is code for doing things that are not necessarily ROS
 related, but are useful for multiple packages. This includes things like the
 ROS_DEPRECATED and ROS_FORCE_INLINE macros, as well as code for getting
 backtraces.

Package: libroscpp-serialization0d
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Robot OS library for roscpp serialization
 This package is part of Robot OS (ROS). It is a C++ library for
 serialization as described in MessagesSerializationAndAdaptingTypes.
 This package is a component of roscpp.

Package: librostime1d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Robot OS library for time and duration
 This package is part of Robot OS (ROS). It contains Time and Duration
 implementations for C++ libraries, including roscpp. ROS has builtin
 time and duration primitive types, which roslib provides as the
 ros::Time and ros::Duration classes, respectively.
